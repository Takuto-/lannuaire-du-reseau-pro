<?php
/*
Plugin Name: Annuaire
Plugin URI: https://mon-siteweb.com/
Description: Ceci est le plugin pour l'affichage de l'annuaire
Author: Simplon
Version: 1.0
Author URI: http://mon-siteweb.com/
*/

function shortcode_tableau(){
    global $wpdb;
    $entreprises = $wpdb->get_results("
    SELECT nom_entreprise,
    localisation_entreprise,
    prenom_contact,
    nom_contact,
    mail_contact
    FROM wp_annuaire;
    ");

    
    echo "<table>";
    echo "<tr>";
    echo "<th>Nom entreprise</th>";
    echo "<th>Localisation</th>";
    echo "<th>Prénom</th>";
    echo "<th>Nom</th>";
    echo "<th>Mail</th>";
    echo "</tr>";
    foreach($entreprises as $entreprise) {
       echo '<tr>';
       echo "<td>".$entreprise->nom_entreprise."</td>";
       echo "<td>".$entreprise->localisation_entreprise."</td>";
       echo "<td>".$entreprise->prenom_contact."</td>";
       echo "<td>".$entreprise->nom_contact."</td>";
       echo "<td>".$entreprise->mail_contact."</td>";
       echo '</tr>';
    }
    echo '</table>';
}
add_shortcode('tableau', 'shortcode_tableau');
?>
